	\context ChordNames
		\chords {
		\set chordChanges = ##t

		% intro
		g1:7

		% aleluya, aleluya...
		c1 g1 a1:m e1:m
		f1 c1 g1 g1:7

		\bar "||"

		% aleluya, su victoria...
		c1 g1 a1:m e1:m
		f1 c1 g1 g1:7

		% amen...
		f1 g1 c1
		}
