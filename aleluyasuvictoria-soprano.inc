\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key c \major

		R1  |
		r4 g' 8 g' g' 4 g'  |
		g' 4 f' e' d'  |
		r4 e' 8 e' e' 4 e'  |
%% 5
		e' 4 d' c' b  |
		r4 c' 2 d' 4  |
		c' 2. ( e' 4 )  |
		g' 1 ~  |
		g' 1  |
%% 10
		r4 g' 8 g' g' 4 g'  |
		g' 4 f' e' d'  |
		r4 e' 8 e' e' 4 e'  |
		e' 4 d' c' b  |
		r4 c' 8 c' c' 4 d'  |
%% 15
		c' 2. ( e' 4 )  |
		g' 1 ~  |
		g' 1  |
		f' 1 (  |
		g' 1 )  |
%% 20
		g' 1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "soprano" {
		¡A -- le -- lu -- ya, a -- le -- lu -- ya!
		¡A -- le -- lu -- ya, a -- le -- lu -- ya!
		¡A -- le -- lu __ ya! __

		"(¡A" -- le -- lu -- "ya!)" Su vic -- to -- ria
		"(¡A" -- le -- lu -- "ya!)" se pro -- cla -- ma
		en to -- da la tie __ rra. __

		A __ mén.
	}

>>
